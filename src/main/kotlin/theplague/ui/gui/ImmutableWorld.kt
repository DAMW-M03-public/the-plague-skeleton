package theplague.ui.gui

import theplague.interfaces.*


class ImmutableWorldData(
    val width: Int,
    val height: Int,
    val territories: List<List<ImmutableTerritory>>,
    val player: ImmutablePlayer,
    val gameFinished : Boolean,
)

fun IWorld.toImmutableWorld() = ImmutableWorldData(width, height, territories.toImmutableTerritories(), player.toImmutablePlayer(), gameFinished())

class ImmutablePlayer(
    override val turns: Int,
    override val livesLeft: Int,
    override val currentWeapon: Iconizable,
    override val currentVehicle: Iconizable
) : IPlayer

fun IPlayer.toImmutablePlayer() = ImmutablePlayer(turns, livesLeft, currentWeapon, currentVehicle)

fun List<List<ITerritory>>.toImmutableTerritories() = map{ row-> row.map { cell-> cell.toImmutableTerritory() }}

class ImmutableTerritory(val iconList : List<ImmutableIconizable>) : ITerritory {
    override fun iconList(): List<Iconizable> = iconList
}

fun ITerritory.toImmutableTerritory() = ImmutableTerritory(iconList().map { it.toImmutableIconizable() })

class ImmutableIconizable(override val icon: String) : Iconizable

fun Iconizable.toImmutableIconizable() = ImmutableIconizable(icon)

